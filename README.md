# Recharge Items

**Compatibility:** Classic Doom source ports supporting ACS and DECORATE.

Once picked up, the Recharge Item regens up to a certain amount of Health or Armor, periodically. These items can go beyond the conventional Health or Armor limits, up to 250 each. Their effects also persist after map change. They are listed below:

  - HealthRechargeSphere: regens health, up to 100 points
  - ArmorRechargeSphere: regens armor, up to 100 points
  - HealthAndArmorRechargeSphere: regens health and armor, up to 200 points
  - RechargeMedikit: regens health, up to 25 points
  - RechargeStimpack: regens health, up to 10 points
  - RechargeMediPatch: regens health, up to 15 points
  - RechargeMediPack: regens health, up to 50 points
  - RechargeSurgicalPack: regens health, up to 75 points
  - RechargeEmergencyHealthRestore: regens health fast, when the player reaches less than 50 HP, up to 100 HP
